from flask import Flask, render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL,MySQLdb

from config import config

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'ldp'
mysql = MySQL(app)

@app.route('/')
def index():
    return render_template('/Index.html')


@app.route('/log', methods=['GET', 'POST'])
def log():
    return render_template('auth/log-in.html')


@app.route('/registro', methods=['GET','POST'])
def registro():

    if(request.method == 'GET'):
       return render_template('auth/registro.html')
    else:
        name = request.form['name']
        email = request.form['email']
        password = request.form['password']

        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO user (fullname, email, password) VALUES (%s,%s,%s)", (name, email, password))
        mysql.connection.commit()
        return render_template(url_for('log-in'))
    
if(__name__=='__main__'):
    app.config.from_object(config['development'])
    app.run()